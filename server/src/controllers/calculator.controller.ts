import { Request, Response } from 'express';
import { Calculation } from '../models/calculation.model';
import { calculate } from '../utils/math.util';


export const calculateResult = (req: Request, res: Response) => {
  const { num1, num2, operator }: Calculation = req.body;
  const result = calculate(num1, num2, operator);
  res.json({ result });
}