import express from 'express';
import { calculateResult } from '../controllers/calculator.controller';

const router = express.Router();

router.post('/api/calculate', calculateResult);

export default router;