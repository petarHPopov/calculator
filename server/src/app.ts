import express from 'express';
import bodyParser from 'body-parser';
import calculatorRoutes from './routes/calculator.routes';
import cors from 'cors';

const app = express();

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

app.use(bodyParser.json());

app.use(cors());

app.use(calculatorRoutes);

const port = 3000;
app.listen(port, () => {
  console.log(`The server is listening on a port ${port}`);
});

