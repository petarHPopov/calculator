
export const calculate = (num1: number, num2: number, operator: string): number => {
    let result: number;
    switch (operator) {
      case '+':
        result = num1 + num2;
        break;
      case '-':
        result = num1 - num2;
        break;
      case '*':
        result = num1 * num2;
        break;
      case '/':
        result = num1 / num2;
        break;
      default:
        result = NaN;
    }
    return result;
  }