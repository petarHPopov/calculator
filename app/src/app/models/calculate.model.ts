export interface ICalculate {
  num1: number | null,
  num2: number,
  operator: string;
}
