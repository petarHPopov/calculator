import {Component, OnDestroy, OnInit} from '@angular/core';
import {select, Store} from "@ngrx/store";
import {getResult} from "./store/calculator.selectors";
import {Subscription} from "rxjs";
import {loadResult} from "./store/calculator.actions";


@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit, OnDestroy {

  currentNumber: string = '0';
  firstNumber: number | null = null;
  operator: string | null = null;
  waitForSecondNumber = false;

  result$ = this.store.pipe(select(getResult));
  resultSub: Subscription = Subscription.EMPTY;

  private result: number | null = null;

  constructor(private store: Store) {
  }

  ngOnInit(): void {
  }

  public getNumber(v: string) {
    if (this.waitForSecondNumber) {
      this.currentNumber = v;
      this.waitForSecondNumber = false;
    } else {
      this.currentNumber === '0' ? this.currentNumber = v : this.currentNumber += v;
    }
  }

  public getDecimal() {
    if (!this.currentNumber?.includes('.')) {
      this.currentNumber += '.';
    }
  }

  private calculate(op: string, secondNumber: number) {

    this.store.dispatch(loadResult({num1: this.firstNumber, num2: secondNumber, operator: op}));

    this.resultSub = this.result$.subscribe((data: any) => {
      if (data) {
        this.result = data['result'];
        this.currentNumber = String(this.result);
        this.firstNumber = this.result;
      }
    })
  }

  public getOperation(op: string) {
    if (this.firstNumber === null) {

      this.firstNumber = Number(this.currentNumber);

    } else if (this.operator && op === '=') {

      this.calculate(this.operator, Number(this.currentNumber));
    }
    this.operator = op;
    this.waitForSecondNumber = true;
  }

  public clear() {
    this.currentNumber = '0';
    this.firstNumber = null;
    this.operator = null;
    this.waitForSecondNumber = false;
    this.result = null;
  }

  ngOnDestroy() {
    this.resultSub.unsubscribe();
  }
}
