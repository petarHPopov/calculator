import {CalculatorComponent} from "./calculator.component";
import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {CalculatorService} from "../../core/calculator.service";
import {CalculatorEffects} from "./store/calculator.effects";
import {calculatorReducer} from "./store/calculator.reducer";
import {HttpClientModule} from "@angular/common/http";

export const calculatorRoutes: Routes = [
  {
    path: '',
    component: CalculatorComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(calculatorRoutes),
    StoreModule.forFeature('calculator', calculatorReducer),
    EffectsModule.forFeature([
      CalculatorEffects,
    ]),
  ],
  declarations: [
    CalculatorComponent
  ],
  exports: [

  ],
  providers: [
    CalculatorService
  ],

})
export class CalculatorModule {

  constructor() {

  }


}
