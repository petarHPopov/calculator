import {createReducer, on} from "@ngrx/store";
import {loadResult, loadResultSuccess} from "./calculator.actions";

export interface CalculatorState {
  result: number;
}

export const initialState: CalculatorState = {
  result: 0,
}

export const calculatorReducer = createReducer(
  initialState,
  on(loadResult, (state) => {
    return {...state}
  }),
  on(loadResultSuccess, (state, result) => {
    return {...state, ...result}
  }),
)
