import {createAction, props} from "@ngrx/store";

export const loadResult = createAction('[Calculator] Load Result',
  props<{num1: number | null, num2: number, operator: string}>());
export const loadResultSuccess = createAction('[Calculator] Load Result Success',
  props<{result: number}>());
