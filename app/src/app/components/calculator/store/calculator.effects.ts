import {Actions, createEffect, ofType} from "@ngrx/effects";
import {CalculatorService} from "../../../core/calculator.service";
import {Injectable} from "@angular/core";
import {Action} from "@ngrx/store";
import {loadResult, loadResultSuccess} from "./calculator.actions";
import { map, catchError, mergeMap} from 'rxjs/operators';
import {of} from "rxjs";
class EffectError implements Action {
  readonly type = '[Error] Effect Error CalculatorEffects';
}

@Injectable()
export class CalculatorEffects {

  constructor(private actions$: Actions, private readonly service: CalculatorService) {
  }

  loadResult$ = createEffect(() => this.actions$.pipe(
    ofType(loadResult),
    mergeMap(({num1, num2, operator}) => this.service.calculate(num1, num2, operator)
      .pipe(
        map((result) => {
          return loadResultSuccess({result});
        }),
        catchError((err) => of(new EffectError()))
      ))
  ));
}
