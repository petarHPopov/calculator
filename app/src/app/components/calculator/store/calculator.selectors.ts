import {createFeatureSelector, createSelector} from "@ngrx/store";
import {CalculatorState} from "./calculator.reducer";

export const getCalculatorStore = createFeatureSelector<CalculatorState>('calculator');

export const getResult = createSelector(getCalculatorStore, r => r.result);
