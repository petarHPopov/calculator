import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {ICalculate} from "../models/calculate.model";

@Injectable()
export class CalculatorService {
  private apiUrl = 'http://localhost:3000/api/calculate';

  constructor(private http: HttpClient) { }

  calculate(num1: number | null, num2: number, operator: string): Observable<number> {
    const payload: ICalculate = { num1, num2, operator };

    return this.http.post<number>(this.apiUrl, payload);
  }

}
